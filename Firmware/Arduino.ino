// firmware V4.6 for mutantC v4
// install thes libs before building the firmware
#include "Arduino.h"
#include <hidcomposite.h>
#include <Keypad.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>
#include <string>
#include "cdcusb.h" //https://github.com/chegewara/EspTinyUSB v2.0.0 with esp board v 2.0.1

// mutantC variables
#define FIRMWARE_VERSION 4.6
#define MUTANTC_VERSION   4 
#define EEPROM_SIZE 1

// ESP32-S2 Pin Assignments:
#define LED_T           5
#define BUZZER          40
#define POWER_MAIN      38
#define POWER_OFF_BTN   26
#define PI_STATE        41
#define BATTERY_M       1
#define VIBRATION       45
#define LDR             17
#define POWER_EXP       37
#define POWER_LCD       39

// Enable(1) / Disable(0) mutantC Features:
#define LDR_AUTO_OFF     0  // automatically turns of the screen in darkness;
#define CHK_PI_STATE     0  // automatically shuts off ESP if Pi is off;

// Specify what minimum value from the LDR to consider as 'darkness':
#define LDR_THRESHOLD 300
// Specify low voltage value that will trigger an alert (LED blink):
#define BATT_LOW_VOLTAGE 3.0
// Turn off the device when it's below this vlotage
#define BATT_TRESHOLD_VOLTAGE 2.8

// alert_LED status values (unsigned int):
#define LED_NONE         0  // LED off, no errors
#define LED_LOW_BATT     1  // LED red & flashing, battery below threshold;
#define LED_LAYOUT       2  // LED red & flashing, battery below threshold;
#define LED_LCD_OFF      3  // LED red & flashing, battery below threshold;

// Set the inteval between battery+temperature checks in milliseconds:
#define SAFETY_CHECK_INT 10000

// Thumbstick, set pin numbers for the four buttons:
#define T_RIGHT   8
#define T_UP      10
#define T_LEFT    9
#define T_DOWN    4

// To read ESP32's built-in temperature sensor (in degrees F):
// This doesn't work on Arduino IDE:
//extern uint8_t temprature_sens_read();

// Switch Stats
int expansionState = 1;         // the current state of the output pin

// Indicates the alert type
int led_alert = 0;

// For Notification LED
Adafruit_NeoPixel ledThumb( 1, LED_T, NEO_GRB + NEO_KHZ800 );


/* --- SECTION: KEYPAD + THUMBSTICK ---------------------------------------- */
HIDcomposite HIDdevice;

// For Thumbstick
int range = 2;              // output range of X or Y movement; affects movement speed
int responseDelay = 10;     // response delay of the mouse, in ms

// For Keypad
const byte KBD_ROWS = 5;
const byte KBD_COLS = 11;
static bool keymapState = 0;

// Keypad event codes:
#define KMAP_SWITCH             0x92
#define EXPANSION_SWITCH        0x93
#define LCD_SWITCH              0x94
#define KMOUSE_LEFT             0x96
#define KMOUSE_RIGHT            0x97
#define HARDWARE_TEST           0x99


// You can change the keys and add from this list https://github.com/hathach/tinyusb/blob/master/src/class/hid/hid.h#L356-L526
static uint8_t keymapAlpha[] =
{
  HID_KEY_GUI_LEFT,     HID_KEY_CAPS_LOCK,     HID_KEY_TAB,   HID_KEY_ESCAPE, HID_KEY_COPY, HID_KEY_PASTE,  HID_KEY_F1, EXPANSION_SWITCH, HARDWARE_TEST,  KMAP_SWITCH,    LCD_SWITCH,
  HID_KEY_ARROW_RIGHT,   HID_KEY_1,             HID_KEY_2,     HID_KEY_3,      HID_KEY_4,    HID_KEY_5,      HID_KEY_6,  HID_KEY_7,        HID_KEY_8,      HID_KEY_9,      HID_KEY_0,
  HID_KEY_ARROW_LEFT,  HID_KEY_Q,             HID_KEY_W,     HID_KEY_E,      HID_KEY_R,    HID_KEY_T,      HID_KEY_Y,  HID_KEY_U,        HID_KEY_I,      HID_KEY_O,      HID_KEY_P,
  KMOUSE_LEFT,          HID_KEY_A,             HID_KEY_S,     HID_KEY_D,      HID_KEY_F,    HID_KEY_G,      HID_KEY_H,  HID_KEY_J,        HID_KEY_K,      HID_KEY_L,      HID_KEY_BACKSPACE,
  KMOUSE_RIGHT,         HID_KEY_CONTROL_LEFT,  HID_KEY_Z,     HID_KEY_X,      HID_KEY_C,    HID_KEY_V,      HID_KEY_B,  HID_KEY_N,        HID_KEY_M,      HID_KEY_SPACE,  HID_KEY_ENTER        
};

static uint8_t keymapSymbols[] =
{
  HID_KEY_GUI_LEFT,     HID_KEY_CAPS_LOCK,  HID_KEY_TAB,        HID_KEY_ESCAPE,           HID_KEY_COPY,             HID_KEY_PASTE,          HID_KEY_F2,             EXPANSION_SWITCH,   HARDWARE_TEST,    KMAP_SWITCH,    LCD_SWITCH,
  HID_KEY_ARROW_RIGHT,   HID_KEY_1,          HID_KEY_2,          HID_KEY_3,                HID_KEY_4,                HID_KEY_5,              HID_KEY_6,              HID_KEY_7,          HID_KEY_8,        HID_KEY_9,      HID_KEY_0,
  HID_KEY_ARROW_LEFT,  'E',                HID_KEY_ARROW_UP,   HID_KEY_PAGE_UP,          'E',                      HID_KEY_BRACKET_LEFT,   HID_KEY_BRACKET_RIGHT,  HID_KEY_SLASH,      HID_KEY_GRAVE,    'E',            'E',
  KMOUSE_LEFT,          'E',                HID_KEY_ARROW_DOWN, HID_KEY_PAGE_DOWN,        'E',                      HID_KEY_APOSTROPHE,     HID_KEY_MINUS,          HID_KEY_SEMICOLON,  HID_KEY_EUROPE_1, 'E',            HID_KEY_BACKSPACE,
  KMOUSE_RIGHT,         HID_KEY_SHIFT_LEFT, HID_KEY_KEYPAD_ADD, HID_KEY_KEYPAD_SUBTRACT,  HID_KEY_KEYPAD_MULTIPLY,  HID_KEY_KEYPAD_DIVIDE,  HID_KEY_EQUAL,          HID_KEY_COMMA,      HID_KEY_PERIOD,   HID_KEY_SPACE,  HID_KEY_ENTER
};

static char dummyKeypad[KBD_ROWS][KBD_COLS] = {
  { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10},
  {11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21},
  {22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32},
  {33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43},
  {44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54}
};

byte kbd_row_pins[KBD_ROWS] = { 3, 2, 11, 12, 13 };                     //connect to the row pinouts of the keypad
byte kbd_col_pins[KBD_COLS] = { 7, 35, 21, 33, 34, 42, 16, 18, 14, 15, 6 }; //connect to the column pinouts of the 

//initialize an instance of class NewKeypad
Keypad Keyboard  = Keypad( makeKeymap( dummyKeypad ), kbd_row_pins, kbd_col_pins, KBD_ROWS, KBD_COLS );

void thumbstick( void )
{
  // disable the thumb stick when display is off
  if( !digitalRead( POWER_LCD ) ) return;
  
  // read the thumbstick buttons:
  int upState = digitalRead( T_UP );
  int downState = digitalRead( T_DOWN );
  int rightState = digitalRead( T_RIGHT );
  int leftState = digitalRead( T_LEFT );

  // calculate the movement distance based on the button states:
  int  xDistance =( leftState - rightState ) * range;
  int  yDistance =( upState - downState ) * range;

  // if X or Y is non-zero, move:
  if( ( xDistance != 0 ) ||( yDistance != 0 ) )
  {
    HIDdevice.move( xDistance, yDistance );
  }
}

void mouse_left( void )
{
  if( !digitalRead( POWER_LCD ) ) return;
  HIDdevice.pressLeft();
  delay( 20 ); 
}

void mouse_right( void )
{
  if( !digitalRead( POWER_LCD ) ) return;
  HIDdevice.pressRight();
  delay( 20 );
}

static void switch_Keymap( void )
{
  if( keymapState ){
    keymapState = 0;
    led_alert = LED_NONE;
  }
  else{
    keymapState = 1;
    led_alert = LED_LAYOUT;
  }
}

static uint8_t mapKey( char key )
{
  if( keymapState )
    return keymapSymbols[key];

  return keymapAlpha[key];
}

static uint8_t modifierKey = 0;

static void processKeys(void)
{
  if (!Keyboard.getKeys())
    return;

  if (!digitalRead(POWER_LCD))
    return;
   
  int i;

  for (i = 0; i < LIST_MAX; i++) {
    if (!Keyboard.key[i].stateChanged)
      continue;

    uint8_t key = mapKey(Keyboard.key[i].kchar);

    if (key == KMAP_SWITCH || key == KMOUSE_LEFT || key == KMOUSE_RIGHT || key == EXPANSION_SWITCH || key == LCD_SWITCH || key == HARDWARE_TEST)
      continue;

    switch (Keyboard.key[i].kstate) {
    case PRESSED: {
        if (key == HID_KEY_CONTROL_LEFT) {
          HIDdevice.sendPress(0, KEYBOARD_MODIFIER_LEFTCTRL);
          modifierKey = KEYBOARD_MODIFIER_LEFTCTRL;
        } else if (key == HID_KEY_SHIFT_LEFT ) {
          HIDdevice.sendPress(0, KEYBOARD_MODIFIER_LEFTSHIFT );
          modifierKey = KEYBOARD_MODIFIER_LEFTSHIFT;
        } else if (key == HID_KEY_GUI_LEFT) {
            HIDdevice.sendPress(0, KEYBOARD_MODIFIER_LEFTGUI);
            modifierKey = KEYBOARD_MODIFIER_LEFTGUI;
        } else {
            HIDdevice.sendPress(key, modifierKey);
            modifierKey = 0;
        }
    }
    break;
    case RELEASED: {
      HIDdevice.sendRelease();
      modifierKey = 0;
    }
    break;
    }
  }
}

/* --- END ----------------------------------------------------------------- */

/* --- SECTION: NEOPIXEL LED ----------------------------------------------- */
//  Variables tied to alert_LED():
int colorCount = 0;
int delayCount = 1000;
bool colorIncrease = true; // Will increase the color values or decrease
bool canDelay = false;
static void breathe_LED()
{
  // Breath LED when the LCD is Off
  if (!canDelay) {
      if (colorIncrease) {
          colorCount=colorCount+3.6;
      } else {
          colorCount=colorCount-3.6;
      }

      //Set the new color on the pixel.
      ledThumb.setPixelColor(0,colorCount,colorCount , 0);
      ledThumb.show();

      //Cycle the colors at the end.
      if (colorCount > 254) {
          colorIncrease = false;
          canDelay = false;
      } else if (colorCount <= 1) {
          colorIncrease = true;
          canDelay = true;
      }
  } else {
    delayCount--;
    if (delayCount <= 0) {
      canDelay = false;
      delayCount = 1000;
    }
  }
  delay(25);
}

/**
 * Makes the LED blink to alert mutantC's user of a particular condition:
 *   RED  =  battery low (< 2.8v)
 *   (additional blinking colors can be use to indicate different conditions)
 *
 * Requires global status variables:
 *   led_alert = integer value indicating if there's an alert;
 *                   \__ configured by alert_LED_set(); values:
 *        0 = no alert, LED off;
 *        1 = battery level alert, LED RED;
 *        2 = keyboard layout change, LED GREEN;
 *        3 = LCD off, breath LED YELLOW;
 */
static void alert_LED()
{
  // Check to see what alert status is currently active:
  switch( led_alert )
  {
    // No alert going on, disable LED but only if it's not breathing.
    case LED_NONE:
      ledThumb.setPixelColor( 0, 0, 0, 0 );
      ledThumb.show();

      // breath led
      delayCount = 0;
      colorCount=0;
      canDelay = false;
      colorIncrease = true;
      break;
      
    case LED_LOW_BATT:
      ledThumb.setPixelColor( 0, 255, 0, 0);
      ledThumb.show();
      break;
      
    case LED_LAYOUT:
      ledThumb.setPixelColor( 0, 0, 255, 0);
      ledThumb.show();
      break;
      
    case LED_LCD_OFF:
      breathe_LED();
      break;
    
    // Unknown alert level, abort.
    default:
      ledThumb.setPixelColor( 0, 0, 0, 0 );
      ledThumb.show();
      return;
  }
}


/* --- SECTION: S Y S T E M ------------------------------------------------ */
/**
 * Turn the LCD screen on or off.
 *
 * When the LCD screen is disabled pulse the notification LED to indicate 
 * the power-saving state. Otherwise, turn off the notification LED.
 *
 * @param yes  specifies if the screen should be disabled (true) or not.
 */

/**
 * Disable the LCD screen if it is enabled, otherwise, enable it.
 */
static void lcd_power_toggle( void )
{
  if(digitalRead(POWER_LCD)){
    digitalWrite(POWER_LCD, LOW);
    led_alert = LED_LCD_OFF;
  }else{
    digitalWrite(POWER_LCD, HIGH);
    led_alert = LED_NONE;
  }
  
  Serial.println( "lcd_power_toggle" );
}

/**
 * Automatically turns off the LCD screen when the system's light sensor
 * goes below a specified threshold.
 */
static void lcd_auto_off( void )
{
  // Uncomment the following code to see LDR sensor values:
  //Serial.println( analogRead( LDR ) );
  
  if( analogRead( LDR ) < LDR_THRESHOLD ) lcd_power_toggle( );
  
  Serial.println( "lcd_auto_off" );
}
static void expansion_power_toggle( void )
{
  if( digitalRead( POWER_EXP ) )
  {
    digitalWrite( POWER_EXP, LOW );
    EEPROM.write( 0, LOW );
    EEPROM.commit();
  }
  else
  {
    digitalWrite( POWER_EXP, HIGH );
    EEPROM.write( 0, HIGH );
    EEPROM.commit();
  }
  
  Serial.println( "expansion_power_toggle" );
}

static void startUpNotification( void )
{
  Serial.println( "notification_buzzer" );
  digitalWrite( BUZZER, HIGH );
  delay( 50 );
  digitalWrite( BUZZER, LOW );
  delay( 50 );
  digitalWrite( BUZZER, HIGH );
  delay( 50 );
  digitalWrite( BUZZER, LOW );
  digitalWrite( VIBRATION, HIGH );
  delay( 100 );
  digitalWrite( VIBRATION, LOW );
  delay( 90 );
  digitalWrite( VIBRATION, HIGH );
  delay( 120 );
  digitalWrite( VIBRATION, LOW );
}

static void hardware_test( void )
{
  Serial.println( "BUZZER" );
  digitalWrite( BUZZER, HIGH );
  delay( 50 );
  digitalWrite( BUZZER, LOW );
  delay( 50 );

  Serial.println( "VIBRATION" );
  digitalWrite( VIBRATION, HIGH );
  delay( 100 );
  digitalWrite( VIBRATION, LOW );
  delay( 90 );

  Serial.println( "LDR" );
  Serial.println( analogRead(LDR) );
  delay( 120 );
  Serial.println( analogRead(LDR) );
  delay( 120 );

  Serial.println( "PI_STATE" );
  Serial.println( digitalRead(PI_STATE) );
  
  Serial.println( "POWER_EXP" );
  digitalWrite( POWER_EXP, LOW );
  delay( 120 );
  digitalWrite( POWER_EXP, HIGH );
  delay( 120 );
  
  Serial.println( "POWER_LCD" );
  digitalWrite( POWER_LCD, LOW );
  delay( 120 );
  digitalWrite( POWER_LCD, HIGH );
  delay( 120 );
}

static void power_off( void )
{
  uint8_t i;

  /* Ask RPi to powerdown */
  HIDdevice.sendPress( HID_KEY_POWER );

  /* Wait for some time */
  for( i = 0; i < 20; i++ ) {
    Serial.println( "power_off_countdown" + i );
  }
  delay( 16000 );
  /* And finally turn off the power */
  Serial.println( "power_off" );
  digitalWrite( POWER_MAIN, LOW );
}

/**
 * Checks to see if the Pi is active by checking the PI_STATE pin.
 * When it becomes LOW, it signals that the Pi was shutdown so mutantC will 
 * cut power to it.
 */
static void process_pi_state( void )
{
  // Check if Pi is active. If so, do nothing.
  if( digitalRead( PI_STATE ) ) return;
  
  // Pi is inactive/shut down, shut down mutantC.
  power_off();
}

static uint8_t exp_board_power_get( void )
{
  return !digitalRead( POWER_EXP );
}

static uint8_t lcd_power_get( void )
{
  return !digitalRead( POWER_LCD );
}

static void keypadEvent( KeypadEvent key )
{
  if( Keyboard.getState() != PRESSED ) return;

  uint8_t kcode = mapKey( key );
  
  switch( kcode )
  {
    case KMAP_SWITCH:
      switch_Keymap();
      break;
    case HARDWARE_TEST:
      hardware_test();
      break;
    case KMOUSE_LEFT:
      mouse_left();
      break;
    case KMOUSE_RIGHT:
      mouse_right();
      break;
    case EXPANSION_SWITCH:
      expansion_power_toggle();
      break;
    case LCD_SWITCH:
      lcd_power_toggle();
      break;
  }
}

/**
 *  Fix code to turn off buzzer after the given interval has passed;
 */
static void start_buzzer( uint16_t interval_ms )
{
  digitalWrite( BUZZER, HIGH );
  //  wait for the specified miliseconds then turn off buzzer.
  delay( interval_ms);
  digitalWrite( BUZZER, LOW );
}

static void lcd_power_set( uint8_t val )
{
  if( val )
    digitalWrite( POWER_LCD, LOW );
  else
    digitalWrite( POWER_LCD, HIGH );
}

static void exp_board_power_set( uint8_t val )
{
  if( val )
    digitalWrite( POWER_EXP, LOW );
  else
    digitalWrite( POWER_EXP, HIGH );
}


/* --- SECTION: Raspberry PI Battery & Temperature ------------------------ */
/* ----- section copied from mutantC v3 firmware: battery+temp functions.
 * Temperature sensor offset should be accordingly to the datasheet between -10 and +10.
 *
 * To callibrate the offset do following:
 *
 * - Let the device sit down powered off at a room temperature for 20 minutes
 * - Turn it on, read temperature using mutantctl
 * - Adjust this constant so that the result matches room temp
 */
#define TEMP_OFFSET -7
// Temperature in Kelvins when to power off 
static uint16_t temp_thresh = 323;    // <-- this needs to be CHANGED!
// Last temperature measured 
static uint16_t last_temp;

// Voltage 5V = 10bit, set to ~2750mV 
static float batt_voltage;

/**
 *  Fixed the following to calculate voltage from raw value
 *  \__ based on the schematic, BATTERY_M is connected between two 100k 
 *       resistors which are connected to the 18650 battery;
 *  \__ this means, the value read by the ADC (ESP32 resolution = 3.3/4095) will 
 *       be half the current voltage provided by the battery, so x2;
 *  \__ 18650 Battery levels: 3v 0%; 3.74v 20%; 3.82v 50%; 4.2v 100%;
 *    see: http://www.benzoenergy.com/blog/post/what-is-the-relationship-between-voltage-and-capacity-of-18650-li-ion-battery.html
 * 20211021.0030 ESP's ADC is set to 13-bit precision so max value is 8192!
 *  \__ used actual values provided by rahman: 
 *       adc reading of 5600 === 3.690v Mult(x5.4) | 5864 === 3.65v (x5.34)
 *
 *  Note: commented out Serial.print() calls. See battery_state()
 */
static float read_voltage( void )
{
  // For Battery Voltage
  float voltage;
  uint32_t  value = 0;
  
  // Get Battery voltage: from Analog Pin w 0 to 4095 range
  value = analogRead( BATTERY_M );
  voltage = value * 5.34 / 8192;
  //Serial.print( "Voltage = " );
  //Serial.println( voltage );
  delay( 200 );
  return voltage; 
}

/**
 *  Read Temperature & Battery Status then test against set thresholds
 *   to determine if a power_off() signal needs to be sent.
 *
 * Also checks to see if the current reading is at or below BATT_LOW_VOLTAGE 
 *   threshold. If it is, send an appropriate alert (blinking red LED).
 */
static void device_safety( void )
{
  // Read temperature from internal sensor & convert to Celcius:
  // \_ changed this to fixed dummy value 0 or Arduino IDE won't compile;
  last_temp = /*( temprature_sens_read() - 32 ) / 1.8 */ 0;
  // Call for power_off() when threshold is exceeded.
  if( last_temp > temp_thresh ) power_off();
    
  // Read current battery level (in volts):
  batt_voltage = read_voltage();
  
  // Check for low battery condition & trigger appropriate alert:
  if( batt_voltage <= BATT_LOW_VOLTAGE ) led_alert = LED_LOW_BATT;;
  
  // Call for power_off() when voltage drops below set threshold.
  if( batt_voltage < BATT_TRESHOLD_VOLTAGE ) power_off();
}

/* --- SECTION: USB Device CDC for Serial Communication w Pi --------------- */
//#if CFG_TUD_CDC
CDCusb USBSerial;
//#endif

/* --- SECTION: S E R I A L / UART Communications ------------------------- */
/* ----- copied from mutantC v3 firmware: UART protocol info...
 *
 * Simple UART protocol for setting and getting data.
 * The message consists of a few bytes followed by a newline.
 *
 * To setup the serial port on RPi use:
 *
 * stty -F /dev/ttyACM0 9600 raw -clocal -echo icrnl
 *
 * Then you can read results and write commands with:
 *
 * cat /dev/ttyACM0
 * cat > /dev/ttyACM0
 *
 * cmd[0]:
 * ? - get value
 * ! - set value
 *
 * cmd[1]
 * T - internal temperature in Kelvin
 * P - poweroff threshold temperature in Kelvin
 * V - mutant hardware revision
 * B - battery voltage (1024 == 5V)
 * Q - poweroff threshold battery voltage (1024 == 5V)
 * L - display led
 * M - body led
 * F - firmware version
 * Z - buzzer turn on for n ns
 *
 * l - LCD screen power
 * e - expansion board power
 * s - sensors power
 *
 * cmd[2] - cmd[4]
 *  - optional command parameter
 *
 * '\n' - message end
 */

/** 
 * A function to send messages back to the Raspberry Pi.
 *
 * @param  type  the type code prefix.
 * @param  val   the float value to send.
 */
static void serial_write( char type, float val )
{
  USBSerial.write( type );
  USBSerial.print( val, 3 );  // send the value w/ 3 decimal places;
  USBSerial.write( '\n' );
}

/**
 * A function to process commands sent via the serial port.
 * Modified & streamlined by Toph v1.20210901 for mutantC v4
 * Recieved command strings must be in the form: 
 *    <command><variable>[<value>]
 * Examples: 
 *    query: "?T", "?B"; set: "!P45", "!Q20", "!L1", "!M2";
 *
 * Modified by Toph v2.20211001 to process data from USB CDC;
 */
static void process_serial( String cmd = "" )
{
  // cmd = placeholder for the command sent via serial/ttl
  //static String cmd;
  // placeholder for a single character/setting value digit
  static uint8_t in;

  // no string given, get data from serial object (for backward compatibility);
  if( cmd.length() == 0 )
  {
    // nothing to process, so exit the function
    if( !Serial.available() ) return;
    
    // get the command string:
    cmd = Serial.readString();
  }
  
  // Debugging code: shows what command arrived for processing...
  //Serial.printf( "process_serial( \"%s\" );\n", cmd );
  
  // do nothing if it's an invalid command( min 2 characters )
  if( cmd.length() < 2 ) return;
  
  // get the command code:
  in = cmd.charAt( 0 );
  // do nothing if it's an invalid command( at least 3 if command is "!"/set )
  if( in == '!' && cmd.length() < 3 ) return;

  // process the recieved command
  if( in == '?' )
  {
    switch( cmd.charAt( 1 ) ) 
    {
      case 'T':
        serial_write( 'T', last_temp );
        break;
      case 'P':
        serial_write( 'P', temp_thresh );
        break;
      case 'V':
        serial_write( 'V', MUTANTC_VERSION );
        break;
      case 'B':
        serial_write( 'B', batt_voltage );
        break;
      case 'Q':
        serial_write( 'Q', BATT_TRESHOLD_VOLTAGE );
        break;
      case 'F':
        serial_write( 'F', FIRMWARE_VERSION );
        break;
      case 'L':
      case 'M': // there is no "body LED" so same
        serial_write( cmd.charAt( 1 ), digitalRead( LED_T ) );
        break;
      case 'Z':
        serial_write( 'Z', digitalRead( BUZZER ) );
        break;
      case 'l':
        serial_write( 'l', lcd_power_get() );
        break;
      case 'e':
        serial_write( 'e', exp_board_power_get() );
        break;
      /* Not Applicable to mutantC v4----------
      case 's':
        serial_write( 's', sensors_power_get() );
        break;
        ---------------------------------------*/
    }
  }
  else if( in == '!' )
  {
    // get the value & convert it to a number
    in = cmd.charAt( 2 ) - '0';
    // Toph Note: the following code is required since !P & !Q can be 2+ digit numbers
    int n = cmd.length();
    if( n > 3 )
    {
      for( int c = 3;  c < n && cmd.charAt( c ) != '\n';  ++c )
        in = in * 10 + ( cmd.charAt( c ) - '0' );
    }
    // determine which variable to set
    switch( cmd.charAt( 1 ) ) 
    {
      case 'P':
        temp_thresh = in;
        break;
//      case 'Q':
//        BATT_TRESHOLD_VOLTAGE = in;
//        break;
//      case 'L':
//        if( in )
//          digitalWrite( LED_T, HIGH );
//        else
//          digitalWrite( LED_T, LOW );
//        break;
//      case 'M':
//        switch( in ) 
//        {
//          case 0:
//            digitalWrite( LED_T, LOW );
//            break;
//          case 1:
//            digitalWrite( LED_T, HIGH );
//            break;
//      /* Not Applicable to mutantC v4----------
//          case 2:
//            start_led();
//            break;
//        ---------------------------------------*/
//        }
        break;
      case 'Z':
        start_buzzer( in );
        break;
      case 'l':
        lcd_power_set( in );
        break;
      case 'e':
        exp_board_power_set( in );
        break;
      /* Not Applicable to mutantC v4----------
      case 's':
        sensors_power_set( in );
        break;
        ---------------------------------------*/
    }
  }
}

/* --- SECTION: USB Device CDC for Serial Communication w Pi --------------- */
//#if CFG_TUD_CDC
/**
 * mutantC callback class for USB events (connection, data recieved, etc);
 */
class MutantCallbacks : public CDCCallbacks 
{
  void onCodingChange( cdc_line_coding_t const* p_line_coding )
  {
    int bitrate = USBSerial.getBitrate();
    Serial.printf( "new bitrate: %d\n", bitrate );
  }

  bool onConnect( bool dtr, bool rts )
  {
    Serial.printf("connection state changed, dtr: %d, rts: %d\n", dtr, rts);
    return true;  // allow to persist reset, when Arduino IDE is trying to enter bootloader mode
  }

  /**
   * Triggered when data is recieved through the USB serial connection:
   */
  void onData()
  {
    int len = USBSerial.available();
    //Serial.printf( "\nnew data, len %d\n", len ); // <-- for debugging
    
    // buffer to hold incoming commands from the RPi:
    uint8_t cmd[len] = {};
    
    // read the command code sequence into the buffer:
    USBSerial.read( cmd, len );
    
    // process the desired command; originally in UART function above:
    process_serial( String( (char*)cmd ) );
    //Serial.write( cmd, len );
  }

  void onWantedChar( char c )
  {
    Serial.printf( "wanted char: %c\n", c );
  }
};
//#endif


/* --- SECTION: M I C R O C O N T R O L L E R --  M A I N ----------------- */
/**
 * Microcontroller Initialization:
 */
void setup( void )
{  
  /*  Turn on the FET that powers on the board and RPI first */
  pinMode( POWER_MAIN, OUTPUT );
  digitalWrite( POWER_MAIN, HIGH );
  
  // Serial setup
  Serial.begin( 115200 );
  Serial.println( "start" );

  // USB CDC Serial Device
  // \_ Note: when the USBSerial object fails to start, a flag should be set;
  USBSerial.setCallbacks( new MutantCallbacks() );
  if( !USBSerial.begin() )
    Serial.println( "Failed to start CDC USB stack" );
  
  // initialize EEPROM with predefined size
  EEPROM.begin( EEPROM_SIZE );

  // Set PIN Stater
  pinMode( LED_T, OUTPUT );
  pinMode( POWER_OFF_BTN, INPUT_PULLUP );
  pinMode( POWER_EXP, OUTPUT );
  pinMode( POWER_LCD, OUTPUT );
  pinMode( BUZZER, OUTPUT );
  pinMode( VIBRATION, OUTPUT );
  pinMode( BATTERY_M, INPUT );
  pinMode( LDR, INPUT );
  pinMode( PI_STATE, INPUT_PULLUP );
  pinMode( T_UP, INPUT_PULLUP );
  pinMode( T_DOWN, INPUT_PULLUP );
  pinMode( T_LEFT, INPUT_PULLUP );
  pinMode( T_RIGHT, INPUT_PULLUP );

  // Set Switch State
  digitalWrite( POWER_EXP, LOW );
  digitalWrite( BUZZER, LOW );

  analogReadResolution( 13 );
  lcd_power_toggle();  

  // read the last LED state from flash memory
  expansionState = EEPROM.read( 0 );
  // set the LED to the last stored state
  digitalWrite( POWER_EXP, expansionState );

  // Set up Keyboard and Mouse HID
  HIDdevice.begin();

  // Set LED State
  ledThumb.setBrightness( 150 );
  ledThumb.begin();

  // Set up keypad matrix
  Keyboard.setHoldTime( 1 );
  Keyboard.setDebounceTime( 0 );
  Keyboard.addEventListener( keypadEvent );
  delay( 1000 );

  startUpNotification();
}

static uint16_t powered_on = 600;
unsigned long last_batTemp_check = millis();

/**
 * Microcontroller Tasks:
 */
void loop( void )
{
  /* Ignore the power_off button just after it has been pressed to turn the power on */
  if( powered_on > 0 )
  {
    powered_on--;
  } 
  else
  {
    if( !digitalRead( POWER_OFF_BTN ) )
    {
      Serial.println( "POWER_OFF_BTN" );
      power_off();
    }
  }
  
  // Handle mouse interactions:
  thumbstick();

  // Deal with keyboard interactions:
  processKeys();

  // Check to see if the Pi is inactive & if so, power off.
  if( CHK_PI_STATE ) process_pi_state();

  // Disable the LCD screen when the sensor detects darkness:
  if( LDR_AUTO_OFF ) lcd_auto_off();
  
  // Read battery level & temperature every few milliseconds:
  if( ( millis() - last_batTemp_check ) > SAFETY_CHECK_INT )
  {
    device_safety();
    last_batTemp_check = millis();
  }

  // Handle alert notifications (blinking LED) if set:
  alert_LED();
}


// written by
//   Toph @toph2814
//   Cyril Hrubis @metan-ucw
//   Abrar @s96Abrar
//   rahmanshaber @rahmanshaber
